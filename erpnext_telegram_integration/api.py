import frappe
import telegram
bot = telegram.Bot(token=frappe.db.get_value("Telegram Settings","Trueview","telegram_autoreply_token"))
# import requests
import re


@frappe.whitelist(allow_guest = True)
def set_value_test(**kwargs):
    kwargs=frappe._dict(kwargs)
    # frappe.db.set_value("Telegram Replies","1079064831","response",str(kwargs))
    # return "Ok"
    
    # if message:
    telegram_msg = kwargs['message']['text']
    re_string = r"^stock:[a-zA-Z0-9 -]+:M$"
    
    if re.match(re_string,telegram_msg):
        list1 = telegram_msg.split(":")
        item = list1[1]
        # warehouse_name = frappe.db.get_value("Warehouse",{"warehouse_name":warehouse[1]},"name")
        if(frappe.db.exists('Item', item)):
            item_stock = frappe.db.get_value("Bin",{"warehouse":"Mumbai - T","item_code":item},"actual_qty") if frappe.db.get_value("Bin",{"warehouse":"Mumbai - T","item_code":item},"actual_qty") else 0
            reply = "Item Stock is {}".format(item_stock)
        else:
            reply = "Please Enter Correct Item!"
    else:
        reply = "To Get Stock Info Type => stock:item_code:M"


    chat_id = kwargs['message']['chat']['id']
    # reply = message['text']
    # msg_id = kwargs['message']['message_id']
    bot.sendMessage(chat_id=chat_id, text=reply)

        
    return 1
@frappe.whitelist()
def get_chat_id():
    return 1
